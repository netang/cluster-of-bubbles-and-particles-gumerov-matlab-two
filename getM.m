function M = getM( a, vr )
%This function calculate M matrice
global rho_l m
N = length(a);

M11 = zeros(N, N);
for i = 1:N
    for j = 1:N
        if i ~= j
            r = get_r(vr, i,j);
            M11(i, j) = a(j)^2/sqrt(sum(r.*r));
        else
            M11(i, j) = a(i);
        end
    end
end

M12 = zeros(N, 3*N);
for i = 1:N
    for j = 1:N
        if i ~= j
            M12(i, 3*j - 2:3*j) = 1/2*a(j)^3*sqrt(sum(r.*r))^(-3)*get_r(vr, i,j);
        else
            M12(i, 3*j - 2:3*j) = [0 0 0];
        end
    end
end

M21 = zeros(3*N, N);
for i = 1:N
    for j = 1:N
        if i ~= j
            r = get_r(vr, i,j);
            M21(3*i-2:3*i, j) = (m(i) - 4/3*pi*rho_l*a(i)^3)*...
                                        a(j)^2*sqrt(sum(r.*r))^(-3)*r;
        else
            M21(i, j) = 0;
        end
    end
end

M22 = zeros(3*N, 3*N);
for i = 1:N
    for j = 1:N
        for n = 1:3
            for k = 1:3
                if n == k
                    delta = 1;
                else
                    delta = 0;
                end
                if i ~= j
                    r = get_r(vr, i, j);
                    M22(3*i-3+n, 3*j-3+k) = 1/2*(m(i) - 4/3*pi*rho_l*a(i)^3)*...
                       a(j)^3*(-sqrt(sum(r.*r))^(-3)*delta + 3*sqrt(sum(r.*r))^(-5)*r(n)*r(k));
                else
                    M22(3*i-3+n, 3*j-3+k) = (m(i) + 2/3*pi*rho_l*a(i)^3)*delta;
                end                
            end
        end

    end
end

 M12 = zeros(size(M12));
 M21 = zeros(size(M21));
% M22 = zeros(size(M22));

M = [ M11 M12; M21 M22 ];
rank(M)

M


end

