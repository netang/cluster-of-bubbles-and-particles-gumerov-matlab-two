function r_ij = get_r( r, i, j )
% function return r_{ij}
% r_{ij} = r_i - r_j   ---  characterize distance betwen bubble centers (|r_{ij}|)
r_ij = r(3*i-2:3*i) - r(3*j-2:3*j);

end

