
global N m;
global k R01 R02 ap d rho_l rho_p V02 P0 gamma sigma Pg0 Ap omega;
N = 2;
c = 1;
k = 30; %��� ������ k, ��� ������ ��������� ����� ��������� � ��������
R01 = 1e-5; % ������ ���������
R02 = c*(1e-5); % ������ �������
ap = R02;
d = R01+R02+k*R01; %��������� �� ������ �������� �� ������ �������
rho_l = 1000; % ��������� ��������
rho_p = 0;%2000; % ��������� �������, rho1 �� ����������, �.�. ������������ ��.
V02 = (4/3)*pi*R02^3; %�����  ������� (� ������ ������ �� �� ��������)
P0 = 1e+5; %����������� �������� (�������� � ��������)
gamma = 1.4; %����������� ���������
sigma = 0.073;
Pg0 = P0+2*sigma/R01;
Ap = P0/2;
nu = 200e+3;
omega = 2*pi*nu;
NT = 5;
tmax = NT/nu;
Nt=5001;
Toutput=linspace(0, tmax, Nt);
time = Toutput*nu;

init;

A1 = zeros(3*N, 3*N);
for i = 1:N
   A1(3*i-2, 3*i-2) = 1/(m(i)-4/3*pi*rho_l*a(i)^3);
   A1(3*i-1, 3*i-1) = 1/(m(i)-4/3*pi*rho_l*a(i)^3);
   A1(3*i, 3*i)     = 1/(m(i)-4/3*pi*rho_l*a(i)^3);
end

A2 = zeros(3*N, 3*N);
for i = 1:N
   A2(3*i-2, 3*i-2) = (2*pi*rho_l*a(i)^3)/(m(i)-4/3*pi*rho_l*a(i)^3);
   A2(3*i-1, 3*i-1) = (2*pi*rho_l*a(i)^3)/(m(i)-4/3*pi*rho_l*a(i)^3);
   A2(3*i, 3*i)     = (2*pi*rho_l*a(i)^3)/(m(i)-4/3*pi*rho_l*a(i)^3);
end


%init s and p from (68)
M = getM(a, r);
% sp = [eye(N)         M(1:N, N+1:end)    *(eye(3*N)-A2^(-1)*A1);
%       zeros(3*N,N)   M(N+1:end, N+1:end)*(eye(3*N)-A2^(-1)*A1)]\(M*[w; -A2^(-1)*u]) ;
sp = [eye(N)         M(1:N, N+1:end)    *(-A2^(-1)*A1);
      zeros(3*N,N)   (eye(3*N)-M(N+1:end, N+1:end)*A2^(-1)*A1)]\(M*[w; -A2^(-1)*u]) ;

s = sp(1:N);
p = sp(N+1:end);
%==========================


options = odeset('RelTol',1e-4);
[T,Y] = ode45(@solver,Toutput,[s; p; a; r], options);
[T_RP1,Y_RP1] = ode45(@SimpleSystem,Toutput,[R01 0], options);
[T_RP2,Y_RP2] = ode45(@RayleighPlesset,Toutput,[R01 0], options);
figure;
plot(T*nu,  Y(:, 10)/R01, T_RP1*nu, Y_RP1(:, 1)/R01, T_RP2*nu, Y_RP2(:, 1)/R01,  time_mas, Vg_mas(:,1).^(1/3), 'LineWidth',2 );
legend('R_b(t) Gumerov', 'R(t) Releigh-Plesset', 'R(t) Releigh-Plesset with Berknes', 'BEM');
% saveflag = 0;
% set(0,'DefaultAxesFontSize',14);
% 
% figure;
% plot(time, abs(Y1D(:,3)),   time, abs(Y4(:,3)), 'LineWidth',2 );
% legend('U = |U_1| = |U_2| Todd 1D (equation 9)',     'U = |U_1| = |U_2| N.A. 1D (equation 11)');
% if saveflag ~= 0
%     saveas(gcf,'fig8.png');
% end;
get_u;